// Define pin numbers
const int soilMoisturePin = A1;  // Soil moisture sensor analog pin
const int tempSensorPin = A0;    // Temperature sensor analog pin
const int relayPin = 2;          // Relay module for water pump
const int redLEDPin = 5;         // Red LED pin
const int blueLEDPin = 4;        // Blue LED pin
const int fanMotorPin = 3;       // Fan motor pin
const int waterPumpMotorPin = 6; // Water pump motor pin

// Define thresholds
const int moistureThreshold = 400; // Adjust based on your sensor's calibration
const int hotTempThreshold = 20;   // Example hot temperature threshold in Celsius
const int coldTempThreshold = 15;  // Example cold temperature threshold in Celsius

void setup() {
  // Initialize serial communication for debugging
  Serial.begin(5000);

  // Initialize the pins
  pinMode(relayPin, OUTPUT);
  pinMode(redLEDPin, OUTPUT);
  pinMode(blueLEDPin, OUTPUT);
  pinMode(tempSensorPin, INPUT);
  pinMode(fanMotorPin, OUTPUT);
  pinMode(waterPumpMotorPin, OUTPUT);

  // Ensure the relay, LEDs, and motors are off at the start
  digitalWrite(relayPin, LOW);
  digitalWrite(redLEDPin, LOW);
  digitalWrite(blueLEDPin, LOW);
  digitalWrite(fanMotorPin, LOW);
  digitalWrite(waterPumpMotorPin, LOW);
}

void loop() {
  // Read the soil moisture level
  int soilMoistureValue = analogRead(soilMoisturePin);
  Serial.print("Soil Moisture Value: ");
  Serial.println(soilMoistureValue);

  // Read the temperature value (assuming TMP36 sensor)
  int tempValue = analogRead(tempSensorPin);
  float voltage = tempValue * (5.0 / 1023.0);
  float temperatureC = (voltage - 0.5) * 100;
  Serial.print("Temperature: ");
  Serial.print(temperatureC);
  Serial.println(" C");

  // Determine if watering is needed
  if (soilMoistureValue < moistureThreshold) {
    // Turn on the relay (activate the water pump)
    digitalWrite(relayPin, HIGH);
    // Turn on the water pump motor
    digitalWrite(waterPumpMotorPin, HIGH);
  } else {
    // Turn off the relay (deactivate the water pump)
    digitalWrite(relayPin, LOW);
    // Turn off the water pump motor
    digitalWrite(waterPumpMotorPin, LOW);
  }

  // Temperature control logic
  if (temperatureC > hotTempThreshold) {
    // Temperature is too hot
    digitalWrite(redLEDPin, HIGH);  // Turn on red LED
    digitalWrite(blueLEDPin, LOW);  // Turn off blue LED
  } else if (temperatureC < coldTempThreshold) {
    // Temperature is too cold
    digitalWrite(redLEDPin, LOW);   // Turn off red LED
    digitalWrite(blueLEDPin, HIGH); // Turn on blue LED
  } else {
    // Temperature is within a normal range
    digitalWrite(redLEDPin, LOW);   // Turn off red LED
    digitalWrite(blueLEDPin, LOW);  // Turn off blue LED
  }

  // Control fan motor based on LED states
  if (digitalRead(redLEDPin) == HIGH) {
    digitalWrite(fanMotorPin, HIGH); // Turn on fan motor
  } else if (digitalRead(blueLEDPin) == HIGH) {
    digitalWrite(fanMotorPin, LOW);  // Turn off fan motor
  } else {
    digitalWrite(fanMotorPin, LOW);  // Default: turn off fan motor
  }

  // Add a delay for stability
  delay(1000);
}
